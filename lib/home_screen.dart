import 'package:asl_video_player/asl_video_player.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 2,
        child: NestedScrollView(
          headerSliverBuilder: (context, value) {
            return [
              SliverAppBar(
                centerTitle: true,
                title: Text("Video player demo"),
                // Tab bar...
                bottom: TabBar(
                  tabs: [
                    Tab(text: "Local video player"),
                    Tab(text: "Netwrok video player"),
                  ],
                ),
              ),
            ];
          },
          // Tab bar view...
          body: TabBarView(
            children: [
              // Local video player
              Center(
                child: FlatButton.icon(
                  icon: Icon(Icons.video_collection),
                  label: Text("Pick Videos"),
                  onPressed: () async {
                    try {
                      PickedFile pickedFile = await ImagePicker()
                          .getVideo(source: ImageSource.gallery);
                      if (pickedFile.path.isNotEmpty) {
                        // Push to video player
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (ctx) => VideoPlayer(
                              videoUrl: pickedFile.path,
                              videoPlayType: VideoPlayType.Local,
                            ),
                          ),
                        );
                      }
                    } catch (e) {
                      print('Local video player error : $e');
                    }
                  },
                ),
              ),
              // Network video player
              FlatButton.icon(
                icon: Icon(Icons.video_collection),
                label: Text("Play network video"),
                onPressed: () {
                  try {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (ctx) => VideoPlayer(
                          videoUrl:
                              "https://www.radiantmediaplayer.com/media/big-buck-bunny-360p.mp4",
                          videoPlayType: VideoPlayType.NetWork,
                        ),
                      ),
                    );
                  } catch (e) {
                    print('Online video player error : $e');
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
